const fs = require('fs')

let input = fs.readFileSync('./day-1-input.txt')
input = input.toString().split('\n').filter(s => s.length).map(s => parseInt(s, 10))

const answer1 = input.reduce((accum, current) => accum + current, 0)
console.log('answer 1:', answer1)

const freqs = new Set()
let firstRepeat
let i = 0
let accum = 0
while (true) {
  accum += input[i]
  if (freqs.has(accum)) {
    firstRepeat = accum
    break
  }
  freqs.add(accum)
  i += 1
  if (i >= input.length) {
    i = 0
  }
}

console.log('answer 2:', firstRepeat)
