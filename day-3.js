const fs = require('fs')

let input = fs.readFileSync('./day-3-input.txt')
input = input.toString().split('\n').filter(s => s.length)

const claimRx = new RegExp(/^#(\d+)\s+@\s+(\d+),(\d+):\s+(\d+)x(\d+)$/)
const claims = input.map((s) => {
  const match = claimRx.exec(s)
  return {
    id: parseInt(match[1], 10),
    x: parseInt(match[2], 10),
    y: parseInt(match[3], 10),
    width: parseInt(match[4], 10),
    height: parseInt(match[5], 10),
  }
})

const totalWidth = Math.max(...claims.map(c => c.x + c.width))
const totalHeight = Math.max(...claims.map(c => c.y + c.height))

const rasterBuffer = (new Array(totalWidth * totalHeight)).fill(0)

function rasterize(x, y) {
  rasterBuffer[(y * totalWidth) + x] += 1
}

claims.forEach((claim) => {
  for (let x = claim.x; x < (claim.x + claim.width); x += 1) {
    for (let y = claim.y; y < (claim.y + claim.height); y += 1) {
      rasterize(x, y)
    }
  }
})

const overSoldInches = rasterBuffer.filter(i => i > 1)
console.log('answer 1:', overSoldInches.length)

function doesIntersect(claim1, claim2) {
  return (claim1.x < claim2.x + claim2.width &&
    claim1.x + claim1.width > claim2.x &&
    claim1.y < claim2.y + claim2.height &&
    claim1.height + claim1.y > claim2.y)
}

let noOverlap
claims.forEach((first) => {
  let intersects = false
  claims.forEach((second) => {
    if (first === second) {
      return
    }
    if (doesIntersect(first, second)) {
      intersects = true
    }
  })

  if (!intersects) {
    noOverlap = first
  }
})

console.log('answer 2:', noOverlap.id)
