const { loadInput } = require('./util')

const input = loadInput(4)
const scheduleRx =
  /^\[(\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2})]\s+(falls\sasleep|wakes\sup|Guard\s#(\d+)\sbegins\sshift)$/
const entries = input.map((s) => {
  const match = scheduleRx.exec(s)
  const timestamp = `${match[1]}${match[2]}${match[3]}${match[4]}${match[5]}`
  return {
    timestamp,
    minute: parseInt(match[5], 10),
    message: match[6],
    guardId: parseInt(match[7], 10)
  }
})
  .sort((a, b) => a.timestamp - b.timestamp)

const guards = {}
let currentGuard
let asleepMin
entries.forEach((entry) => {
  if (entry.message.startsWith('Guard')) {
    currentGuard = guards[entry.guardId]
    if (!currentGuard) {
      currentGuard = {
        total: 0,
        id: entry.guardId,
        minutes: new Proxy({}, {
          get: (target, name) => name in target ? target[name] : 0
        }),
      }
      guards[entry.guardId] = currentGuard
    }
  } else if (entry.message === 'falls asleep') {
    asleepMin = entry.minute
  } else if (entry.message === 'wakes up') {
    const duration = entry.minute - asleepMin
    currentGuard.total += duration
    for (let i = asleepMin; i < entry.minute; i += 1) {
      currentGuard.minutes[i] += 1
    }
  }
})

const mostAsleepGuard = Object.values(guards).sort((a, b) => b.total - a.total)[0]
const sleepiestMinute = Object.entries(mostAsleepGuard.minutes).sort(((a, b) => b[1] - a[1]))[0][0]
console.log('answer 1:', mostAsleepGuard.id * sleepiestMinute)

let mostAsleepMinute = 0
let mostAsleepCount = 0
let mostAsleepMinuteGuard

Object.values(guards).forEach((guard) => {
  const minuteData = Object.entries(guard.minutes).sort(((a, b) => b[1] - a[1]))[0]
  if (!minuteData) {
    return
  }
  const minute = parseInt(minuteData[0], 10)
  const count = parseInt(minuteData[1], 10)
  if (count <= mostAsleepCount) {
    return
  }

  mostAsleepMinuteGuard = guard
  mostAsleepMinute = minute
  mostAsleepCount = count
})

console.log('answer 2:', mostAsleepMinuteGuard.id * mostAsleepMinute)
