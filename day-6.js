const { defaultObject, Grid } = require('./util')

const coords = [[162, 168], [86, 253], [288, 359], [290, 219], [145, 343], [41, 301], [91, 214], [166, 260],
  [349, 353], [178, 50], [56, 79], [273, 104], [173, 118], [165, 47], [284, 235], [153, 69], [116, 153], [276, 325],
  [170, 58], [211, 328], [238, 346], [333, 299], [119, 328], [173, 289], [44, 223], [241, 161], [225, 159], [266, 209],
  [293, 95], [89, 86], [281, 289], [50, 253], [75, 347], [298, 241], [88, 158], [40, 338], [291, 156], [330, 88],
  [349, 289], [165, 102], [232, 131], [338, 191], [178, 335], [318, 107], [335, 339], [153, 156], [88, 119], [163, 268],
  [159, 183], [162, 134]]

const gridSizeX = 360
const gridSizeY = 360

function taxiDistance(x1, y1, x2, y2) {
  return Math.abs(x2 - x1) + Math.abs(y2 - y1)
}

const coordMap = new Grid(gridSizeX, gridSizeY, -1)
const onEdge = new Set()

for (let x = 0; x < gridSizeX; x += 1) {
  for (let y = 0; y < gridSizeY; y += 1) {
    let closestCoord = 0
    let closestCoordDistance = 999999

    coords.forEach((coord, coordIndex) => {
      const distance = taxiDistance(x, y, coord[0], coord[1])
      if (distance < closestCoordDistance) {
        closestCoordDistance = distance
        closestCoord = coordIndex
      } else if (distance === closestCoordDistance) {
        closestCoord = -1
      }
    })

    coordMap.set(x, y, closestCoord)
    if (x === 0 || x === (gridSizeX - 1) || y === 0 || y === (gridSizeY - 1)) {
      onEdge.add(closestCoord)
    }
  }
}

const noInfinites = coordMap.buffer.map(v => onEdge.has(v) ? -1 : v).filter(v => v !== -1)
const areaSize = defaultObject(0)
noInfinites.forEach(v => areaSize[v] += 1)
const largestArea = Object.values(areaSize).sort((a, b) => b - a)[0]

console.log('answer 1:', largestArea)

for (let x = 0; x < gridSizeX; x += 1) {
  for (let y = 0; y < gridSizeY; y += 1) {
    const totalDistance = coords.reduce((accum, coord) => accum + taxiDistance(x, y, coord[0], coord[1]), 0)
    coordMap.set(x, y, totalDistance < 10000 ? 1 : 0)
  }
}
const withinDistance = coordMap.buffer.filter(v => v)
console.log('answer 2:', withinDistance.length)
