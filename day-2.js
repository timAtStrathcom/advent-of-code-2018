const fs = require('fs')

let input = fs.readFileSync('./day-2-input.txt')
input = input.toString().split('\n').filter(s => s.length)

let has2Count = 0
let has3Count = 0

input.forEach((s) => {
  const letters = s.split('')
  const letterCount = new Proxy({}, {
    get: (target, name) => name in target ? target[name] : 0
  })

  letters.forEach((l) => { letterCount[l] += 1 })
  const uniqueCounts = new Set(Object.values(letterCount))
  if (uniqueCounts.has(2)) {
    has2Count += 1
  }
  if (uniqueCounts.has(3)) {
    has3Count += 1
  }
})

console.log('answer 1:', has2Count * has3Count)

let answer2
input.forEach((first) => {
  const firstLetters = first.split('')
  input.forEach((second) => {
    const secondLetters = second.split('')

    let diffIndex = -1
    for (let i = 0; i < first.length; i += 1) {
      if (firstLetters[i] !== secondLetters[i]) {
        if (diffIndex > -1) {
          // this is the second different letter so it's not the one we're looking for
          return
        }
        diffIndex = i
      }
    }

    if (diffIndex > -1) {
      answer2 = first.slice(0, diffIndex) + first.slice(diffIndex + 1)
    }
  })
})

console.log('answer 2:', answer2)
