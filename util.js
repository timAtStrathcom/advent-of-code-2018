const fs = require('fs')

function loadInput(day) {
  const input = fs.readFileSync(`./day-${day}-input.txt`)
  return input.toString().split('\n').filter(s => s.length)
}

function defaultObject(defaultValue) {
  return new Proxy({}, {
    get: (target, name) => name in target ? target[name] : defaultValue
  })
}

class Grid {
  constructor(width, height, fill = 0) {
    this.width = width
    this.height = height
    this.buffer = (new Array(width * height)).fill(fill)
  }

  bufferIndex(x, y) {
    return (y * this.height) + x
  }

  set(x, y, value) {
    this.buffer[this.bufferIndex(x, y)] = value
    return this
  }

  get(x, y) {
    return this.buffer[this.bufferIndex(x, y)]
  }
}

module.exports = {
  loadInput,
  defaultObject,
  Grid
}
